require 'active_model'
require 'require_all'
require 'yaml'

# load all ruby files in the directory "lib" and its subdirectories
require_all 'lib'
require_all 'controllers'
require_all 'data_mappers'
require_all 'models'
require_all 'views'
