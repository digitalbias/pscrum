class App
  attr_reader :config

  def initialize(config_files)
    load_configuration(config_files)
    copy_sqlite_database
  end

  def copy_sqlite_database
    home_dir = File.expand_path('~')
    dest_location = "#{home_dir}#{@config[:sqlite][:working_database]}"
    source_location = "#{home_dir}#{@config[:sqlite][:master_database]}"
    # TODO: Doing this is a security risk, but we're going to do it for now. Fix later
    `cp #{source_location} #{dest_location}`
    # FileUtils.cp(source_location, dest_location) # Why isn't this working?
  end

  def load_configuration(config_files)
    @config = {}
    config_array = config_files.map do |file|
      name = File.basename(file, ".yml")
      data = YAML.load_file(File.expand_path(File.open(file))).inject({}){|memo,(k,v)| memo[k.to_sym] = v; memo}
      @config = @config.merge({name.to_sym => data})
    end
  end
end
