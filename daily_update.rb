#!/usr/bin/env ruby
require_relative 'config/initialize'

app = App.new(["config/arangodb.yml", "config/sqlite.yml"])

mapper = TaskMapper.new
list = mapper.get_data(:sqlite, app.config[:sqlite])
