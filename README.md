# Personal Scrum using Things and ArangoDB

This is mostly a personal learning project. I wanted an excuse to use ArangoDB as a datastore for tasks I have within Things. I'll pump the data from Things into AgangoDB and then use Ruby to pull the data out of ArangoDB in a format I can use on a daily and weekly basis to plan what I will do. I'd also like to generate burn-down charts as well as other statistics to see how I'm doing on what I would like to accomplish in my Personal Scrum adventure.
