require "sqlite3"

class TaskMapper < DataMapper

  def get_sqlite_data(config)
    home_dir = File.expand_path('~')
    db_location = "#{home_dir}#{config[:working_database]}"

    db = SQLite3::Database.new db_location
    rows = db.execute <<-SQL
select ZTITLE, ZDUEDATE, ZTAG, ZAREA, ZPROJECT, ZNOTES, ZOUTCOME
from ZTHING
where ZTRASHED=0
  AND ZSTATUS=0
  AND ZFOCUSLEVEL IS NULL
  AND ZSCHEDULER=1
  AND ZSTART==1
  AND ZTYPE=0
ORDER BY ZTITLE ASC
    SQL
    date = nil
    rows.each do | row |
      if row[1]
        date = Date.new(row[1]).strftime("%Y-%m-%d")
      else
        date = ""
      end
      puts "#{row[0]}, #{date}"
    end
    rows
  end

  def get_data(name, config)
    if name.to_s == "sqlite"
      get_sqlite_data(config)
    end
  end
end
